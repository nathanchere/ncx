# .ncx

Started out as my [.dotfiles](https://gitlab.com/nathanchere/dotfiles) and a few scripts for bootstrapping a new Linux installation but has eventually grown into... well... this.

As always, caveat emptor.

## Quickstart

Assumes `git` already installed.

### Installation

```bash
git clone https://gitlab.com/nathanchere/ncx.git ~/.ncx
cd ~/.ncx
cp ./config.example ./config
# edit the contents of ./config to suit your circumstances
sudo ./install.sh
```

### Deploy dotfiles

```
ncx dots stow
```

See results with `ncx dots status`

Remove pre-existing/conflicting files with `ncx dots clean --force` -- USE WITH CAUTION, you may lose changes you haven't committed / backed up / etc

### Set up development environment

```
# install development tools
ncx setup dev

# install xmonad environment
ncx setup desktop

# misc setup
ncx setup essentials
ncx setup fonts
ncx setup shell
```

## Requirements

### Supported hardware

Things I currently run:

  * Lenovo X1 Nano Gen2
  * Lenovo X1 Nano
  * Lenovo X1 Extreme Gen4

What I've previously used with these scripts:

  * Dell Precision 7530
  * Dell XPS 13 9350
  * Macbook Pro 2013 15"
  * Raspberry Pi 3 Model B / B+
  * Lenovo X1 Extreme Gen2
  * Lenovo X1 Carbon Gen5
  * Dell XPS 13 9360
  * Raspberry Pi Zero W

### Supported / tested distros

I'm running on various arch-based installations. My main daily laptop circa 2023-05 is running EndeavourOS. It has previously been either stock Arch or Antergos. Basically any faithful arch-based distro should be as fine as it can be. Not sure about Manjaro.

The last version that worked with Arch Linux ARM was circa 2018-08 targetting RPi 3B and RPi 3B+.

The last cross-distro version <=0.7 additionally supports Korora 24, 25, 26 and Fedora 25, 26. Not maintained for many years.

Everything assumes your system language is set to some variant of English, otherwise many things will break and I have no interest in fixing these.

## Installation

First clone locally:

    git clone https://gitlab.com/nathanchere/ncx.git ~/.ncx
    cd ~/.ncx

Copy the example config and fill out the placeholders for things like your git
credentials and machine-specific identifiers (hints provided in-line):

    cp ./config.example ./config

Run the bootstrapper

    sudo ./install.sh

This will do a lot of things but some of the main steps of note are:

* clone my dotfiles as a submodule
* initialise a config file for the `ncx` tool with the settings in ./config
* install various common dependencies and Python libraries needed by `ncx`
* configure udev rules
* add `$HOME/.ncx/system/bin` to $PATH via profiles.d

If something breaks unexpectedly try instead running as:

    sudo ./install.sh debug

Once installed, type `ncx` to see what you can do from there.

`ncx help {modulename}` gives further advice on specific modules.

## How things work

The default window manager for this setup is xmonad. You can use others but will lose some (non-essential) functionality.

Session starts up from `system/xsessions/`.

### Environment variables

.ncx main configuration is in `~/.config/.ncx`. On session startup

## Useful / recommended tools

Aside from the `ncx` helper, I often use the following:

* `nmcli` : network manager CLI, useful for easy WiFi management
  - `nmcli device wifi` to list available WiFi connections
  - `nmcli c` to list saved WiFi connections
  - `nmcli c up <name>` to connecto to a saved WiFi connection
  - `nmcli c add con-name <hotspotname> type wifi ifname wlp58s0 ssid <hotspotname>` connect to new hotspot, then `nmcli c up <name>` - wtf this is bullshit... needs to be made less bullshit
* `ncdu` - disk usage explorer
* `remmina` - pretty versatile RDP/VNC/etc-in-one client
* `ranger` - terminal file browser

Some quick/dirty scripts included to work around annoyances:

* weui : reui for wayland. Logs found in ~/.local/log/weui
* uc : convert 4-character unicode value to symbol
* ucc : convert 4-character unicode value to symbol and copy to X clipboard

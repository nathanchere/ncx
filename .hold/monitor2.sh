# Activate primary screen
xrandr --output LVDS-1-1 --auto --primary

# If we find that a screen is connected via VGA, activate it and position it
# to the left of the primary screen.
xrandr | grep 'VGA-1 connected' | ifne xrandr --output VGA-1 --auto --left-of LVDS-1-1

# If we find that a screen is connected via Display Port, activate it and position it
# to the left of the primary screen.
xrandr | grep 'DP-1 connected' | ifne xrandr --output DP-1 --auto --left-of LVDS-1-1

# If we find a screen connected via Display Port, make it the primary screen
# and deactivate the built-in screen... also adjust size of icons
#DP_1_CONNECTED=`xrandr | grep 'DP-1 connected'`
#if [ -n "$DP_1_CONNECTED" ]; then
#  xrandr --output LVDS-1-1 --off
#  xrandr --output DP-1 --auto --primary
#  STALONETRAY_SLOT_SIZE=29
#fi

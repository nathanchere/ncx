# Tips

User locked out / sudo password not accepted

```
faillock --user {yourusername}
# faillock --user {yourusername} --reset
```
#!/usr/bin/env bash

# Usage:
#   Set up a config file
#        cp ./config.example ./config
#   Install
#       ./install.sh

#######################################
#
#  Misc helper functions and bash setup
#
#######################################

set -uo pipefail
IFS=$'\n\t'

getUserName(){
    if [[ $EUID -ne 0 ]]; then
        echo `whoami`
    else
        echo "$SUDO_USER"
    fi
}

# Tips from http://stackoverflow.com/a/25515370/243557
# Print the script name and all arguments to std:err
yell() { echo "$*" 2>&1 |& tee -a "$LOGFILE"; }
# Same as yell but exits with error status
die() { yell "$*"; exit 111; }
# Uses boolean shortcircuit to only die if the specified command failed
try() { "$@" || die "cannot $*"; }

log() {
    printf "\033[1;37m[ \033[1;36m* \033[1;37m] \033[0m$1\033[0m\n"
    echo $@ >> "$LOGFILE"
}

logerror() {
    printf "\033[1;37m[ \033[1;31m! \033[1;37m] \033[31m$1\033[0m\n"
    echo $@ >> "$LOGFILE"
}

requireRoot() {
    [[ "$(whoami)" == "root" ]] || die "This script requires root privileges. Try again with sudo."
}

requireNotRoot() {
    "$(whoami)" != "root" || die "This script should not be run as root."
}

# grant ownership to main user instead of root user
disown() {
    echo "Disowning file: $1"
    chown -R "$USERNAME":users "$1"
}

# Stolen from Redhat /etc/profile
# Append to PATH if not already specified
# $1 - string to add to PATH if not already present
# $2 - if 'after', $1 will be added to the end of PATH instead of the start
pathMunge() {
    if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
}

# $1 - the string to be added (if not already present)
# $2 - the file to add to
addToFileOnce() {
    grep -qF "$1" "$2" || echo "$1" >> "$2"
}

drawTime() {
    date +"%T"
}

drawTimestamp() {
    date +"%y%m%d"
}

# Prompt for Y/N input and return either true (Y) or false (N)
# $1 - prompt text
# e.g.:
#  result=`promptYesNo "Are you sure you want to continue?"`
promptYesNo() {
    while true; do
        read -p "$1 [y/n]: " yn
        case $yn in
            [Yy]* ) return 0 ;;
            [Nn]* ) return 1 ;;
        esac
    done
}

promptInput() {
    read -p "$1: " result
    echo $result
}

# $1- url
# $2- local path to download to
# e.g.
#   download https://someurl.com/somefile.txt ~/installer.sh
download () {
    # -L to follow Github redirects like e.g. Github uses
    curl -L "$1" --create-dirs -o "$2"
}

errorTrap() {
    printf "\n\033[91m*****************************************************************\033[0m\n\n\tError on %s\n\tSomething went wrong; Aboring...\n\n\033[91m*****************************************************************\033[0m\n\n" "$(caller)"
    exit
}
exitTrap() {
    printf "\n\033[1;37m[[ \033[1;31m! \033[1;37m]] \033[0m Script exited prematurely\n"
}
debugTrap() {
    printf "[ * ]\033[93m DEBUG: %s\033[0m [ * ]\n" "$(caller)"
}

if [ "${1:-}" == 'debug' ] ; then
    readonly DEBUG_MODE=true
    set -x
else
    readonly DEBUG_MODE=false
fi

trap errorTrap ERR
trap exitTrap EXIT
$DEBUG_MODE && trap debugTrap DEBUG

#######################################
#
#  .ncx Bootstrapper
#
# Installs the main `ncx` tool and dependencies
#
#######################################

USERNAME=`getUserName`
HOME=`getent passwd "$USERNAME" | cut -d: -f6`
NCXROOT="$HOME/.ncx"
LOGROOT="$NCXROOT/logs"
TMPROOT="$NCXROOT/tmp"

CONFIG_FILE="$HOME/.config/.ncx"
LOGFILE="$LOGROOT/$(basename "$0").$(date +'%y%m%d-%H%M').log"
mkdir -p "$LOGROOT"
rm -f "$LOGFILE"

NCXPATH="$HOME/.ncx/ncx/ncx.py"
BIN_INSTALL_PATH="$HOME/.ncx/system/bin"
GLOBAL_PROFILE_FILE="ncx.profile.sh"

log ""
log "  **************************"
log " **                        **"
log "**    .ncx Bootstrapper     **"
log " **                        **"
log "  **************************"
log ""
log "  -- Logging to: $LOGFILE --\n"

#######################################
#
#  Package manager helpers
#
#######################################

# $1: package name as you would provide it to pacman
# Returns installed package version number, or empty if not installed
installedVersion() {
    if ! isPackageInstalled $1; then
        echo ''
        return
    fi
    
    pacman -Qi "$1" | grep "Version" | cut -d ':' -f 2 | cut -d ' ' -f 2
}

# $1: package name as you would provide it to pacman
# Returns true if specified package is installed, false otherwise
# Will give potentially give false negative if any errors occur :(
# TODO: how to check for non-pacman in arch, eg AUR
isPackageInstalled() {
    log "Checking for package $1"
    pacman -Q "$1" &> /dev/null
}

# Meh, does the job
# $1: package description
# $2: pacman package name
installPackage() {
    package=$2
    
    if isPackageInstalled "$package"; then
        log "Package '$package' ($1) is already installed; skipping..."
        return
    fi
    
    log "Installing $package..."
    pacman --noconfirm -S $package
}

installAurPackage() {
    package=$2
    
    if isPackageInstalled "$package"; then
        log "Package '$package' ($1) is already installed; skipping..."
        return
    fi
    
    log "Installing $package..."
    pacman --noconfirm -S $package
}

#######################################
#
#  Pre-setup validation steps
#
#######################################

detectCorrectPath() {
    if [ "$NCXROOT" != "$(pwd)" ]; then
        logerror "Home: $HOME; running from $(pwd)"
        log " * .ncx installer must be run from '\$HOME/.ncx'. Because reasons."
        die "Exiting..."
    fi
    log "Install path: OK; running from $(pwd)"
}

detectAlreadyInstalled() {
    if [ -f "$CONFIG_FILE" ]; then
        promptYesNo ".ncx has already been installed. Remove and re-install?" || die "Exiting..."
        cleanInstall
    else
        log "No prior install: OK"
    fi
}

detectConfigSourceFile() {
    if [ ! -f "./config" ]; then
        logerror "No config source file found. Creating..."
        gitName=`promptInput "Enter your user name for git"`
        gitEmail=`promptInput "Enter your email address for git"`
        echo "Available network devices:"
        ls /sys/class/net
        wifi=`promptInput "Enter the WiFi device ID"`
        echo "Available backlight devices:"
        ls /sys/class/backlight
        backlight=`promptInput "Enter the backlight device name"`
        
        cfg="./config"
        echo "git_name=$gitName" >> $cfg
        echo "git_email=$gitEmail" >> $cfg
        echo "backlight_controller=$backlight" >> $cfg
        echo "nwifi_controller=$wifi" >> $cfg
    else
        log "Config source file: OK"
    fi
}

detectValidDistro() {
    distro_id='unknown'
    distro_name='unknown'
    distro_family='unknown'
    
    if [ ! -f /etc/os-release ]; then
        die "No /etc/os-release file found - what crazy mong-arse distro are you on?"
    fi
    
    while read line; do
        if [[ ${line} =~ ^ID= ]]; then distro_id=${line//*=}; distro_id=${distro_id//\"}; fi
        if [[ ${line} =~ ^ID_LIKE= ]]; then distro_family=${line//*=}; distro_family=${distro_family//\"}; fi
        if [[ ${line} =~ ^PRETTY_NAME= ]]; then distro_name=${line//*=}; distro_name=${distro_name//\"}; fi
    done < /etc/os-release
    
    if [ $distro_family = 'unknown' ]; then distro_family=$distro_id; fi
    
    [ "$distro_id" != 'manjaro' ] && \
    [ "$distro_id" != 'arch' ] && \
    [ "$distro_family" != 'archlinux' ] && \
    [ "$distro_family" != 'arch' ] && \
    die "Detected $distro_name ($distro_id); $distro_family distros not supported. Arch or GTFO #sorrynotsorry"
    
    log "Distro: OK; detected: ${distro_name} (${distro_family} family)"
}

#######################################
#
#  Main installation steps
#
#######################################

initConfigFile() {
    log "Initialising config at $CONFIG_FILE"
    rm -f "$CONFIG_FILE"
    touch "$CONFIG_FILE"
}

addExtraPaths() {
    log "Configuring \$PATH for bash through $GLOBAL_PROFILE_FILE"
    
    profile_d="/etc/profile.d/$GLOBAL_PROFILE_FILE"
    
    # Re-initialise clean profile.d template
    rm -f "$profile_d"
    cp "system/profile.d/ncx.profile.sh" "$profile_d"
    
    # Add .ncx bin helpers/tools
    addToFileOnce "pathMungeBefore '$BIN_INSTALL_PATH'" "$profile_d"
    
    log "Sourcing profile.d to avoid restart"
    source "$profile_d"
}

# Only packages which are required to run ncx
# End-user software is installed through ncx itself
installPrereqs() {
    log "Installing prerequisite packages"
    installPackage "GNU stow" stow
    installPackage "rsync" rsync
    installPackage "curl" curl
    installPackage "Python 3.x" python
    installPackage "pip" python-pip
    installPackage "Build essentials" base-devel
    
    log "Installing Python libs"
    pip install urwid # for GUI rendering
    pip install configparser
    pip install python-networkmanager
    pip install notify2
}

installUserConfig() {
    log "Configuring miscellaneous user settings"
    
    # add groups and rules for things like user backlight permissions
    rsync -avm "system/udev/" "/etc/udev/rules.d"
    gpasswd -a "$USERNAME" video
    
    #reload newly synced udev rules
    udevadm control --reload-rules
    udevadm trigger
}

installNcxUtil () {
    ln -s "$NCXPATH" "/usr/bin/ncx"
}

initialiseDotfilesRepo() {
    log "Initialising dotfiles submodule"
    git submodule init
    git submodule update
}

finaliseInstallation() {
    echo "Fixing permissions"
    disown "$HOME/.config"
    disown "$NCXROOT"
    
    echo "Fixing permissions"
    cat "./config" >> "$CONFIG_FILE"
    # echo "distro_id=$DISTRO_ID" >> "$CONFIG_FILE"
    # echo "distro_family=$DISTRO_FAMILY" >> "$CONFIG_FILE"
    # echo "distro_package_manager=$DISTRO_PACKAGE_MANAGER" >> "$CONFIG_FILE"
    #echo "git_name=Some Person" >> "$CONFIG_FILE"
    #echo "git_email=" >> "$CONFIG_FILE"
    #echo "backlight_controller=intel_backlight" >> "$CONFIG_FILE"
    echo "complete=1" >> "$CONFIG_FILE"
    log ".ncx bootstrap install complete!"
    log "Type    ncx    to begin your journey down the rabbithole."
}

#######################################
#
#  Misc helpers
#
#######################################

cleanInstall() {
    log "Cleaning existing install..."
    log " * Removing old config file"
    rm -f "$CONFIG_FILE"
    log " * Removing profile.d entry"
    rm -rf "/etc/profile.d/$GLOBAL_PROFILE_FILE"
    log " * Removing ncx util from /usr/bin"
    rm -f "/usr/bin/ncx"
}

#######################################
#
#  Main
#
#######################################

# Pre-install validations
requireRoot

detectValidDistro
detectAlreadyInstalled
detectCorrectPath
detectConfigSourceFile

promptYesNo "Are you sure you want to run the installer?" || die "Exiting..."

initConfigFile
addExtraPaths
installPrereqs
installNcxUtil
installUserConfig
initialiseDotfilesRepo
finaliseInstallation

trap - DEBUG
trap - EXIT

echo "Install complete - type 'ncx' to get started"

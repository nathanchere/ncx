# TODO

hypridle config
hyprlock
yprpapr

https://wiki.archlinux.org/title/Fan_speed_control#ThinkPad_laptops
thinkfan

fix notificatoins not showing f nt critical



## INPSIRATIONS TO CHECK

https://github.com/Syndrizzle/hotfiles
https://www.reddit.com/r/unixporn/comments/uxajy5/worm_starring_eww_as_panel_dashboard_and_the/
https://elkowar.github.io/eww/configuration.html


## TO IMPLEMENT

---

## Snippets

* list monitor support- hyprctl monitors | grep 'Monitor' | awk '{ print $2 }'

## Genreal functionality to integrate

* some kind of opengpt CLI interface
* some kind of update time and timezone on startup

## install.sh

* fix `The directory '/home/norfen/.cache/pip' or its parent directory is not owned or is not writable by the current user.`
* pip install colorama xdg
* AUR install xcursor-pulse-glass wttrbar
* install fonts
  - ttf-mscore-fonts
  - Iosevka Nerd Font

## ncx

* don't just blind-append in common.setConfigFlag'yaourt',
* Excercism key from http://www.exercism.io/account/key
* find better way to ensure all pip dependencies are instlled without needing to run `install.sh` again (or ideally not needing to install them in install.sh at all)

## ncx update

* update when changes on remote (ncx system)
* update when changes on remote (dots)
* sync dot changes to upstream

## ncx setup

* setup display manager (lightdm?)
* support unattended AUR installation (e.g. polybar)
* get latest fonts from git/cdn/etc

## Misc features

* gpg stuff eg https://github.com/jaagr/shellscripts/blob/master/standalone/encryptandsign
* something to help with things like orphaned package cleanup e.g. `pacman -Rns $(pacman -Qtdq)`
* polkit setup
* https://github.com/Pipshag/dotfiles_nord/tree/master/.config nice thin bar style

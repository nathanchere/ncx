#!/usr/bin/env python3

# Things to do:
#  Maintenance
#   fix permissions
#   update from git
#   push updates
#  Reinstall
#   remove itself
#   clone new copy
#   run installer
# install/update/push/fix

from os import path, makedirs

if __name__ != "__main__":
    print("Run, don't import")
    exit(1)

import sys,os
import ncxlib.common as common

# TODO clean this crap up
common.init(os.path.dirname(os.path.realpath(__file__)))

def run(args):
    if len(args) == 1:
        action='help'
        cmdargs = list()
    else:
        cmdargs = list(args)
        cmdargs.pop(0)
        action = cmdargs.pop(0)

    # TODO: can do this cleaner, e.g. modules responsible for
    #  defining their own shortcuts?
    if(action=='kb'): action = 'keyboard'
    if(action=='bl'): action = 'backlight'
    if(action=='wf'): action = 'wifi'

    try:
        exec(f'from ncx_{action} import {action}')
        eval(action + "(cmdargs)")
    except ModuleNotFoundError:
        print(f' *** ERROR *** no such module "{action}"')
        print("Type 'ncx' without arguments for help")
        exit(1)

run(sys.argv)

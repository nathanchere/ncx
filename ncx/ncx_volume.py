if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Volume manager"
help_usage="""
Usage: ncx volume [value]
Manage audio volume. Very primitive, no concern about multiple outputs etc yet.

When called with no [value], displays the current volume

[value] can be in one of the following formats:
\tabsolute          e.g.\tncx volume 666
\trelative          e.g.\tncx volume +40
\tpercentage        e.g.\tncx volume 80%
\trelative %        e.g.\tncx volume -10%
\tset to quietest   i.e.\tncx volume min
\tset to loudest    i.e.\tncx volume max
"""

from ncxlib.common import *

def volume(args):
    from ncxlib.volume import Volume

    # TODO: make configurable on setup

    volume = Volume()

    if(len(args) == 0):
        return volume.show_current()

    value = args[0]
    if(value == 'mute'):
        return volume.toggle_mute()
    if(value == 'max'):
        return volume.set_max()
    if(value == 'min'):
        return volume.set_min()

    operator = '='
    percent = False

    if(value[0] in ['+','-','=']):
        operator = value[0]
        value = value[1:]

    if(value[-1] == '%'):
        value = value[:-1]
        percent = True

    if(not value.isdigit()):
        die(f"Invalid value: {value}")
    value = int(value)

    if(percent):
        if(operator=='='):
            return volume.set_percentage(value)

        currentPercent = volume.current_percentage()
        if(operator=='+'):
            targetPercent = currentPercent + value
        else:
            targetPercent = currentPercent - value

        targetPercent = clamp(targetPercent, 0, 100)
        return volume.set_percentage(targetPercent)

    # Absolute / not-percent
    if(operator=='='):
        return volume.set_absolute(value)

    currentValue = volume.current_value()
    if(operator=='+'):
        targetValue = currentValue + value
    else:
        targetValue = currentValue - value

    return volume.set_absolute(targetValue)

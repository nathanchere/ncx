if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Manage display DPI"
help_usage="""
Usage: ncx dpi [value]
Manage display DPI.

When called with no [value], displays the current DPI

This will only affect newly created UI elements and will
not affect existing windows/etc."""

from ncxlib.common import *
from ncxlib.backlight import Backlight
from tempfile import NamedTemporaryFile

def dpi(args):
    if(len(args) == 0):
        # TODO: nicer output, handle edge cases, handle multiple displays detected
        return show_dpi()

    value = args[0]
    if(not value.isdigit()):
        value = 96
    return set_dpi(value)

def show_dpi():
    # Show monitor hardware settings
    result = subprocess.check_output("xdpyinfo | egrep 'dots per inch|screen #'", shell=True).decode().strip()
    print(result)
    # Show xdrb-configured DPI
    result = subprocess.check_output("xrdb -query | grep Xft.dpi:", shell=True).decode().strip()
    print(result)

def set_dpi(value):
    with NamedTemporaryFile() as temp_file:
        temp_content = f"""! Fonts {{{{{{
Xft.dpi: {value}
! }}}}}}"""
        temp_file.write(temp_content.encode())
        temp_file.flush()
        subprocess.call(f'xrdb -merge {temp_file.name}',shell=True)

    subprocess.call(f'notify-send "Setting DPI to {value}"', shell=True)
    show_dpi()


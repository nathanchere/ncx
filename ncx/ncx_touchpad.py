from ncxlib.common import *
if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short = "Manage touchpad tap-to-click"
help_usage = """
Usage: ncx touchpad [on|off]
Manage touchpad tap-to-click
"""


def touchpad(args):
    from ncxlib.touchpad import Touchpad

    touchpad = Touchpad()

    if(len(args) == 0):
        return print("Specify 1 (on) or 0 (off)")

    value = args[0]
    return touchpad.set_tap_to_click(value)

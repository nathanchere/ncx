if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="dotfile syncing"
help_usage="""
Usage: ncx dots [--verbose] [--force] <command>

Possible commands:
    status (default)  Display current state of managed dotfiles

    diff              Show differences between conflicted dotfiles and live files

    clean             Fix safe blockers like conflicting links
      --force         Also deletes conflicting files (not just unlinks)

    deploy            Deploy stowed dotfile packges as symlinks

    sync              Keep dotfiles synchronised with remote repository

    stow              Import files from live file system into dotfiles
    adopt             Same as `stow` 
      {package}       The name of the dotfile package to import files to
      {target}        The file to import. Currently does not support directories

    edit              Open the dotfile packages folder in vscodium
"""

from ncxlib.common import *
import os,subprocess

def dots(args):
    from ncxlib.stow import Stow

    isForce = popArg(args, ['--force','-f'])
    isVerbose = popArg(args, ['--verbose','-v'])
    stow = Stow(verbose=isVerbose, force=isForce)

    if len(args) == 0:
        return stow.showStatus()

    command = args.pop(0)

    if(command=='clean'):
        return stow.clean()

    if(command=='diff'):
        return stow.diff()

    if(command=='sync'):
        return stow.sync()
    
    if(command=='edit'):
        # run vscodium in the dotfiles folder
        subprocess.run(['vscodium', '~/.ncx/'], check=True)

    if(command=='stow' or command=='deploy'):
        return stow.stow()

    if(command=='adopt'):
        if(len(args) < 2):
            return print("Must specify package name and files to adopt")
        return stow.adopt(args)

    return stow.showStatus()

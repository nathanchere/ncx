if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Lock the current session"
help_usage="""
Usage: ncx lock [options]

Possible options:
    -t, --transparent     Don't blur session in the background
"""

from ncxlib.common import *
import subprocess

def lock(args):
    isTransparent = popArg(args, ['--transparent','-t'])

    if(isTransparent):
        return subprocess.call('pyxtrlock', shell=True)

    return subprocess.call('i3lockr --blur 50 -- --nofork --ignore-empty-password', shell=True)

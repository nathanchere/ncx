if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="display basic environment and diagnostic info"
#help_usage=""" """

from ncxlib.common import *
import subprocess

def info(args):
    return subprocess.call(f'screenfetch', shell=True)

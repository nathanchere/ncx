if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Some basic debugging info"
help_usage="""
Usage: ncx debug
For now just provides a sanity check for the current environment.
"""

from ncxlib.common import *
import subprocess

def debug(args):
    print("-- [ ncx globals ]--")
    for key in ['username','userhome','configpath','ncxpath','ncxroot']:
        print(f"{key.ljust(24)}: {eval(key)}")
    print("config")

    print("-- [ ncx config ]--")
    for key in config:
        print(f" .{key.ljust(22)}: {config[key]}")

    print("-- [ ncx config env exports ]--")
    subprocess.call('printenv | grep NCX_', shell=True)


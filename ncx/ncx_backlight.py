if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Manage laptop monitor backlight brightness level"
help_usage="""
Usage: ncx backlight [value]
Manage laptop monitor backlight brightness level

When called with no [value], displays the current backlight value

[value] can be in one of the following formats:
\tabsolute          e.g.\tncx backlight 666
\trelative          e.g.\tncx backlight +40
\tpercentage        e.g.\tncx backlight 80%
\trelative %        e.g.\tncx backlight -10%
\tset to darkest    i.e.\tncx backlight min
\tset to brightest  i.e.\tncx backlight max
"""

from ncxlib.common import *

def backlight(args):
    from ncxlib.backlight import Backlight

    # TODO: make configurable on setup

    backlight = Backlight()

    if(len(args) == 0):
        return backlight.show_current()

    value = args[0]
    if(value == 'max'):
        return backlight.set_max()
    if(value == 'min'):
        return backlight.set_min()

    operator = '='
    percent = False

    if(value[0] in ['+','-','=']):
        operator = value[0]
        value = value[1:]

    if(value[-1] == '%'):
        value = value[:-1]
        percent = True

    if(not value.isdigit()):
        die(f"Invalid value: {value}")
    value = int(value)

    if(percent):
        if(operator=='='):
            return backlight.set_percentage(value)

        currentPercent = backlight.current_percent()
        if(operator=='+'):
            targetPercent = currentPercent + value + 1
        else:
            targetPercent = currentPercent - value + 1

        targetPercent = clamp(targetPercent, 0, 100)
        return backlight.set_percentage(targetPercent)

    # Absolute / not-percent
    if(operator=='='):
        return backlight.set_absolute(value)

    currentValue = backlight.current_value()
    if(operator=='+'):
        targetValue = currentValue + value
    else:
        targetValue = currentValue - value

    return backlight.set_absolute(targetValue)

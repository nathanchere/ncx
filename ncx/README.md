# ncx

Not all magic is bad.

Hides much of the day-to-day complexity and annoyances of living in the command line. A sanity-preserving CLI for those of us who aren't Rainman.

This is ultimately only for my own benefit and not intended for public consumption, but I've had enough people express interest that I'll make some effort to provide enough documentation to explain WTF is going on at a high level.

I make no claims of being any good with Python, so if you find yourself cringing when looking at this code and are willing to share any tips on what I can do better, I welcome the advice.

## Development notes

Though this is an at times tightly-coupled part of my [.ncx](https://gitlab.com/nathanchere/ncx) Swis-army-knife, for the purposes of documenting the Python-based CLI component of .ncx, I will refer to `.ncx/ncx` as the root. So for example this `README.md` actually refes to `.ncx/ncx/README.md`.

Entry point for the application as a whole is expected to always be `ncx.py`.

Functionality is implemented as modules as found in `ncx_*.py`. Consider these top-level models to be similar in concept to MVC controllers. They should do very little on their own, rather orchestrate other code.

`ncxlib/` is where most of the meaningful functionality is kept. Names should be fairly self-explanitory and each lib is mostly narrow and isolated in scope. `ncxlib/common.py` is the only significant exception which contains helper code used throughout ncx.

### modules

Each module is expected to adhere to the following loose interface:

- `help_short`: summary displayed in `ncx help` module directory
- `help_usage`: detailed help displayed with `ncx help [modulename]`.
- `def [modulename](args):`: the sole entry point for the module. The name of this main method should match the module name, e.g. for `ncx_crypto.py` the main method should be declared `def crypto(args):`.

Accepting args in the method signature is non-optional, though you don't need to do anything with them if you don't want to. You'll see a simple pattern repeated throughout for handling arguments (assuming `ncx_example.py`):

```
def example(args):
  if(len(args) == 0):
    print("No arguments provided")
    exit(0)

  command = args[0]

  if(command == x):
    return do_x();

  if(command == y):
    return do_y();
```

If everything worked correctly, you should see your new module show up in the module directory when you type `ncx help`.

Even though `return` values are used throughout the main methods of modules, do not expect them to display or otherwise do anything with them. Return statements are only there to explicitly indicate an exit point from each module. You will need to explicitly print anything you want shown in the terminal or returned as a status code.

Define functions as needed but try to avoid having any code which runs at the top-level of a module without being contained in functions. This includes referencing libs which should be done within the main method rather than at the top of the file, e.g.:

**Wrong**:

```
from ncxlib.wifi import WiFi

def wifi(args):

  # do stuff here
```

**Right**:

```
def wifi(args):
  from ncxlib.wifi import WiFi

  # do stuff here
```

Why? It might not look as 'neat' as all being at the top of the file, but it helps avoid things like a missing pip package or weird bug in a lib preventing a module from being loaded at all or showing up with an error in the `ncx help` module directory.

### libs

The most useful functionality is stored in what will be referred to from here on as simply 'libs', which are in `ncxlib/`.

While not essential, for consistency I try to stick to each lib being implemented in a single class matching its file name and then imported as needed.

Libs can be referenced from modules with `from ncxlib.{filename} import [Filename]`. Note capitalisation. They should be the first thing included after the class definition e.g:

```
def crypto(args):
  from ncxlib.dialog import Dialog

  # do stuff here
```

The only exception (again) being `ncxlib/common.py` which is imported in full (i.e. `from ncxlib.common import *`) rather than importing a single class.

While there may be exceptions, you generally shouldn't need to have libs reference each other. If you do, you possibly have a lib which is doing too much or combining behaviour which better belongs at the module level.

### ncxlib/common

Access the ncx common library from any file by adding `from ncxlib.common import common`. Some of the more useful helper functions it offers:

- `elevate()`
  Detect if the current instance is being run with root privileges. If not, run the current command again as `sudo`. By default `ncx` should not be run as root.

- `run_command(command)`
  Runs *command* through `/bin/sh` and returns anything output to STDOUT.  

-  `promptYesNo(question, default)`
  Like it sats on the tin. Quick way of getting a yes/no answer from the user, which is returned as `True` or `False`. `default` is optional and refers to what is returned if the user doesn't provide an answer. If a `default` is not provided, the user will be prompted until they provide a non-empty answer.

- `popArg(args, flags)`
  Handy way of handling simple flags as arguments to a module. For example:

  ```
  import ncxlib.common as common

  def greeter(args):
    isInEnglish = common.popArg(args, '--english')
    isInSwedish = common.popArg(args, '--swedish')
    isVerbose = common.popArg(args, ['--verbose','-v'])
  ```

  You can pass either a single string or a list of strings to `popArg` as `flags` and it will return *True* if any of the list of flags is matched. Any specified flags are removed from `args` as they are matched.

There are some other less interesting functions to be found which are small enough to be self-documenting.

Aside from the helper moethods it provides, `ncxlib.config` is also how to access ncx configuration. ncx configuration is an extremely simplistic .ini-style key/value store. Example config file:

```
git_name=Nathan Chere
git_email=git@nathanchere.com.au
backlight_controller=intel_backlight
complete=1
```

The only essential value there is `complete=1`. Anything else is fair game. All values are exposed as a dictionary through `ncx.common.config`. For example to access `backlight_controller` from the example config above (after importing `ncx.common)`: `config['backlight_controller']`.

### Example module and lib

#### ncx_greet.py

```
if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="An example module"
help_usage="""
Usage: ncx greet [option]
This is just an example which helps make people feel more welcome

[option]:
\t{hello|hi}:  says hello
\tdrinks:      buys everyone a round
"""

from ncxlib.common import *

def greet(args):
  from ncxlib.greeter import Greeter

  if(len(args) == 0):
    command = 'hello'
  else:
    command = args[0]  

  if(command == 'hello' or command == 'hi'):
    greeter = Greeter()
    return greeter.say_hello()

  if(command == 'drink'):
    greeter = Greeter()
    return greeter.buy_drinks()

  return print("Unrecognised arguments")
```

#### ncxlib/greeter.py

```
from ncxlib.common import *

class Greeter:

    def say_hello(self):
        return print(f"Hello, world!")

    def buy_drinks(self):
      return print("Did someone say 'shots'?")
```

## Known bugs and shortcomings

- This is not intended as a system-wide tool. It is very much user-specific. If you want one install for multiple users, this is not for you.

- `run_command` is not robust. For example it will not capture std_err output, and some programs *cough*git*cough* abuse  STDERR (*yes, I know, His Holiness Torvalds is responsible for both Linux and git, it's still abuse*). If you need any kind of fault tolerance, use `subprocess` directly instead of `run_command`.

## Support

Non-existent. If you find this interesting at all, awesome. If you learn something from it or it inspires you to roll your own CLI tools, even better. If you attempt to take this and use it as-is in your own environment you are even crazier than I look.
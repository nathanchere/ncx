import os
import subprocess
import sys
import traceback
from random import Random
from pathlib import Path
from colorama import Fore, Style
from ncxlib.common import *


class Wallpaper:

    def _get_files(self):
        results = []
        for path in os.listdir(self.wallpaper_root):
            fullpath = os.path.join(self.wallpaper_root, path)
            if os.path.isfile(fullpath):
                results.append(fullpath)
        return results

    def __init__(self, verbose=False, force=False):
        self.verbose = verbose
        self.force = force
        self.wallpaper_root = f'{ncxroot}/resources/wallpaper/ani'
        self.files = self._get_files()

    def get(self):
        result = Random().choice(self.files)
        print(result)
        return result

    # def set(self):
    #     def clean_package(self, package):
    #         results = []
    #         for root, directories, _ in os.walk(package):
    #             for directory in directories:
    #                 target = self.map_source_to_target(
    #                     package, os.path.join(root, directory))

    #                 isLink = os.path.islink(target)
    #                 exists = os.path.exists(target)
    #                 results += [{'target': target,
    #                              'isLink': isLink, 'exists': exists}]
    #                 if(isLink):
    #                     os.unlink(target)

    #         if(self.verbose):

    #             print(f' {Style.BRIGHT}{package}{Style.RESET_ALL}')
    #             for result in results:
    #                 if(result['isLink']):
    #                     print(
    #                         f' - {result["target"]} [{Fore.RED}Symlink{Fore.RESET}]')
    #                 elif(result['exists']):
    #                     print(
    #                         f' - {result["target"]} [{Fore.GREEN}OK{Fore.RESET}]')
    #                 else:
    #                     print(
    #                         f' - {result["target"]} [{Fore.YELLOW}Missing{Fore.RESET}]')
    #             print("")

    # def map_source_to_target(self, package, sourcePath):
    #     subpath = sourcePath[len(package):]
    #     return f"{self.liveRoot}{subpath}"

    # def map_target_to_source(self, package, targetPath):
    #     subpath = targetPath[len(self.liveRoot):]
    #     return f"{self.stowedRoot}/{package}{subpath}"

    # def adopt(self, args):
    #     package = args[0]
    #     target = os.path.abspath(args[1])
    #     stowedName = self.map_target_to_source(package, target)
    #     stowedDirectory = os.path.dirname(stowedName)

    #     if(not os.path.exists(target)):
    #         return print("Cannot find target file to adopt")

    #     if(not os.path.exists(stowedDirectory)):
    #         print(f"Creating {stowedDirectory}")
    #         Path(stowedDirectory).mkdir(parents=True, exist_ok=True)

    #     if(path.isdir(target)):
    #         return print("TODO: support adopting folders")
    #         # print(f'Absorbing:\n  {target}\ninto: \n  {stowedName}')
    #         # os.rename(target, stowedName)
    #     else:
    #         print(f'Absorbing:\n  {target}\ninto: \n  {stowedName}')
    #         os.rename(target, stowedName)
    #         return os.link(stowedName, target)

    def printIfVerbose(self, value):
        if(not self.verbose):
            return
        print(value)

    def printIfNotVerbose(self, value):
        if(self.verbose):
            return
        print(value)

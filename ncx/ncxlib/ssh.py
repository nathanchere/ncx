import os, subprocess

ssh_private_key=os.path.expanduser('~/.ssh/id_rsa')
ssh_public_key=os.path.expanduser('~/.ssh/id_rsa.pub')

def generate_keys(email):
    if(os.path.exists(ssh_private_key) and os.path.exists(ssh_public_key)):
        return print(f"SSH key found at {ssh_private_key}")
    subprocess.call('ssh-keygen -t rsa -b 4096 -C "{email}"', shell=True)
    print(f"Generated new SSH key pair for {email}")

def get_public_key():
    with open(os.path.expanduser(ssh_public_key),'r') as f:
        return f.read()

def get_private_key():
    with open(os.path.expanduser(ssh_private_key),'r') as f:
        return f.read()

import os
import subprocess
import sys
import traceback
from pathlib import Path
from colorama import Fore, Style
from ncxlib.common import *
from pdb import set_trace


__ignored_files = ['.gitignore']

class Stow:

    def __init__(self, verbose=False, force=False):

        self.verbose = verbose
        self.force = force
        self.stowedRoot = f'{ncxroot}/dotfiles'
        self.liveRoot = os.path.expanduser('~')

    def stow(self):

        def stow_package(self, package):
            packageName = self.package_name(package)
            if packageName.startswith('.'):
                print(f'Skipping package {packageName}')
                return

            self.printIfVerbose(
                f"Deploying package {Style.BRIGHT}{packageName}{Style.RESET_ALL}...")

            errorCount = 0
            for root, directories, files in os.walk(package):
                for directory in directories:
                    target = self.map_source_to_target(
                        package, os.path.join(root, directory))

                    if(not os.path.exists(target)):
                        Path(target).mkdir(parents=True, exist_ok=True)
                        continue

                    if(os.path.islink(target)):
                        errorCount += 1

                if(errorCount > 0):
                    raise Exception(
                        f'{errorCount} errors creating directory structure')

                for file in files:
                    if(os.path.basename(file) == '.gitignore'):
                        continue

                    source = os.path.join(root, file)
                    target = self.map_source_to_target(package, source)

                    # OK: nothing exists; create a new symlink
                    if(not os.path.exists(target)):
                        if(os.path.islink(target)):
                            errorCount += 1
                            print(f'{target} exists as a dead link')
                            continue

                        os.symlink(source, target)
                        continue

                    # ERR: physical file already exists in link destination
                    if(not os.path.islink(target)):
                        errorCount += 1
                        print(f'{target} already exists as physical file')
                        continue

                    # OK: already linked correctly, leave as-is
                    existingSource = str(Path(target).resolve())
                    if(existingSource == source):
                        continue

                    # ERR: linked incorrectly
                    print(f'Conflicting link: {target} -> {existingSource}')
                    errorCount += 1
                    continue

            if(errorCount > 0):
                raise Exception(f'{errorCount} errors deploying stowed files')

                #
                # print(f' F:{file}')
                # print(f'D:{directory} F:{file}')
                # target = self.map_source_to_target(package, os.path.join(root, directory))
                #
                # isLink=os.path.islink(target)
                # exists=os.path.exists(target)
                # results += [{'target': target, 'isLink': isLink, 'exists': exists}]
                # if(isLink):
                #     os.unlink(target)

        totalDeployed = 0
        totalError = 0
        for package in self.get_packages():
            try:
                stow_package(self, package)
                totalDeployed += 1
            except Exception as ex:
                if(self.verbose):
                    print(traceback.format_exc())
                else:
                    print(
                        f"Deploying package {Style.BRIGHT}{package}{Style.RESET_ALL}...")
                    print(f"Error: {ex}")
                totalError += 1

        print(
            f"Successfully deployed: {Fore.GREEN}{totalDeployed}{Fore.RESET}")
        print(f"Errors:                {Fore.RED}{totalError}{Fore.RESET}")
        # ; run {Fore.YELLOW}ncx dots clean{Fore.RESET} to resolve

    def sync(self):
        print(" TODO: work out exactly how this will work and what order, e.g.")
        print("pull from remote dots, let git handle conflicts, commit changes?")

    def clean(self):
        notStowed = []

        for package in self.get_packages():
            ok = True
            results = []
            for root, _, files in os.walk(package):
                for file in files:
                    target = self.map_source_to_target(
                        package, os.path.join(root, file))
                    prefix = root[len(self.stowedRoot) + 2 +
                                  len(self.package_name(package)):]
                    if(os.path.exists(target) and not os.path.islink(target)):
                        ok = False
                        results += [{'path': os.path.join(
                            prefix, file), 'missing': False, 'notStowed': True}]
            if(not ok):
                notStowed += [(package, results)]
        
        nonStowedCount=len(notStowed)
        if not notStowed:
            print(f'{Fore.GREEN}No conflicts{Fore.RESET}')
            return

        print(f'Conflicted files: {Fore.RED}{nonStowedCount}{Fore.RESET}')
        for package in notStowed:
            self.clean_conflicts(package)


    # Get list of all stowed packages - typically one per application

    def get_packages(self):
        packages = [f.path for f in os.scandir(
            self.stowedRoot) if f.is_dir()and not f.name.startswith('.')]
        for package in packages:
            yield package

    def map_source_to_target(self, package, sourcePath):
        subpath = sourcePath[len(package):]
        return f"{self.liveRoot}{subpath}"

    def map_target_to_source(self, package, targetPath):
        subpath = targetPath[len(self.liveRoot):]
        return f"{self.stowedRoot}/{package}{subpath}"

    def adopt(self, args):
        package = args[0]
        target = os.path.abspath(args[1])
        stowedName = self.map_target_to_source(package, target)
        stowedDirectory = os.path.dirname(stowedName)

        if(not os.path.exists(target)):
            return print("Cannot find target file to adopt")

        if(not os.path.exists(stowedDirectory)):
            print(f"Creating {stowedDirectory}")
            Path(stowedDirectory).mkdir(parents=True, exist_ok=True)

        if(path.isdir(target)):
            return print("TODO: support adopting folders")
            # print(f'Absorbing:\n  {target}\ninto: \n  {stowedName}')
            # os.rename(target, stowedName)
        else:
            print(f'Absorbing:\n  {target}\ninto: \n  {stowedName}')
            os.rename(target, stowedName)
            return os.link(stowedName, target)

    def diff(self):
        notStowed = []

        for package in self.get_packages():
            ok = True
            results = []
            for root, _, files in os.walk(package):
                for file in files:
                    target = self.map_source_to_target(
                        package, os.path.join(root, file))
                    prefix = root[len(self.stowedRoot) + 2 +
                                  len(self.package_name(package)):]
                    if(os.path.exists(target) and not os.path.islink(target)):
                        ok = False
                        results += [{'path': os.path.join(
                            prefix, file), 'missing': False, 'notStowed': True}]
            if(not ok):
                notStowed += [(package, results)]
        
        nonStowedCount=len(notStowed)
        if not notStowed:
            print(f'{Fore.GREEN}No conflicts{Fore.RESET}')
            return

        print(f'Conflicted files: {Fore.RED}{nonStowedCount}{Fore.RESET}')
        for package in notStowed:
            self.printDiff(package)

    def showStatus(self):
        stowed = []
        notStowed = []
        notSynced = []

        self.printIfVerbose('Fetching changes from remote...')
        subprocess.call(f'git -C "{self.stowedRoot}" fetch', shell=True)
        changes = self.getGitChanges()

        for package in self.get_packages():
            ok = True
            results = []
            for root, _, files in os.walk(package):
                for file in files:
                    target = self.map_source_to_target(
                        package, os.path.join(root, file))
                    prefix = root[len(self.stowedRoot) + 2 +
                                  len(self.package_name(package)):]
                    if(not os.path.exists(target)):
                        ok = False
                        results += [{'path': os.path.join(
                            prefix, file), 'missing': True, 'notStowed': False}]
                    elif(not os.path.islink(target)):
                        ok = False
                        results += [{'path': os.path.join(
                            prefix, file), 'missing': False, 'notStowed': True}]
            if(ok):
                stowed += [package]
            else:
                notStowed += [(package, results)]

        if(self.verbose):
            for item in stowed:
                print(f'{item} [OK]')
            for item in notStowed:
                print(notStowed)
        else:
            nonStowedCount = len(notStowed)
            print(
                f'Synced packages:    {Fore.YELLOW if nonStowedCount > 0 else Fore.GREEN}{len(stowed)}{Fore.RESET}')
            print(
                f'Non stowed files:   {Fore.RED if nonStowedCount > 0 else Fore.GREEN}{nonStowedCount}{Fore.RESET}')
            for package in notStowed:
                packageName = self.package_name(package[0])

                print(f' \ {Style.BRIGHT}{packageName}{Style.RESET_ALL}')
                self.printFiles(package[1])


            changes = {'added': [], 'deleted': [], 'modified': [], 'other': []}
            if all(not changes[key] for key in changes):
                print(f'Out-of-sync files: {Fore.GREEN}0{Fore.RESET}')
            else:
                print(f'Out-of-sync files: ')
                for file in changes['added']:
                    print(
                        f' [+] {file} {Style.DIM}(added locally){Style.RESET_ALL}')
                for file in changes['deleted']:
                    print(
                        f' [-] {file} {Style.DIM}(added remotely){Style.RESET_ALL}')
                for file in changes['added']:
                    print(
                        f' [>] {file} {Style.DIM}(modified){Style.RESET_ALL}')
                for file in changes['other']:
                    print(f' [?] {file} {Style.DIM}(other){Style.RESET_ALL}')

    def package_name(self, package):
        return os.path.basename(package)

    def printIfVerbose(self, value):
        if(not self.verbose):
            return
        print(value)

    def printIfNotVerbose(self, value):
        if(self.verbose):
            return
        print(value)
    
    def getGitChanges(self):
            results = dict(added=[], deleted=[], modified=[], other=[])
            changes = subprocess.check_output(
                f'git -C "{self.stowedRoot}" diff --name-status origin/master', shell=True).strip().decode().splitlines()
            for change in changes:
                detail = change.split('\t')

                if(not '/' in detail[1]):
                    # Things at root folder of repo aren't dotfile packages
                    continue
                if detail[1].startswith('.'):
                    # Ignore folders starting with .
                    # - they aren't dotfile packages
                    continue
                
                
                if(detail[0] == 'M'):
                    results['modified'] += [detail[1]]
                elif(detail[0] == 'A'):
                    results['added'] += [detail[1]]
                elif(detail[0] == 'D'):
                    results['deleted'] += [detail[1]]
                elif(detail[0][0] == 'R'):
                    results['other'] += [detail[1]]
                elif(detail[0] == 'C'):
                    results['other'] += [detail[1]]
                elif(detail[0] == 'U'):
                    results['other'] += [detail[1]]
                else:
                    common.die(f"Unexpected value from git diff: {detail}")

            return results

    def printDiff(self, package, maxLen=5):
        import difflib

        packageName = self.package_name(package[0])
        print(f' \ {Style.BRIGHT}{packageName}{Style.RESET_ALL}')
        for file in package[1]:
            dotfile_path = f'{self.stowedRoot}/{packageName}/{file["path"]}'
            live_path = f'{self.liveRoot}/{file["path"]}'
            print(f'    .dot: {dotfile_path}')
            print(f'    live: {live_path}')

            with open(dotfile_path, 'r') as file1, open(live_path, 'r') as file2:
                diff = difflib.ndiff(file1.readlines(), file2.readlines())

            for l in diff:
                print(len(l))
            # print(diff)
            # delta = ''.join(x[:2] for x in diff)
            # delta = ''.join(x[2:] for x in diff if x.startswith('- '))
            # print(delta)

    def printFiles(self, files, max=20):
        i = 0
        for file in files:
            marker = f'{Fore.YELLOW}?' if file['notStowed'] else f'{Fore.RED}X'
            comment = f' {Style.DIM}(conflict){Style.RESET_ALL}' if file['notStowed'] else ''
            print(f'  |-[{marker}{Fore.RESET}] ~/{file["path"]}{comment}')
            i += 1
            if(i >= max):
                remainingCount = len(files) - i
                print(
                    f"  | ... {Style.BRIGHT}{remainingCount}{Style.RESET_ALL} more")
                print("")
                return
        print("")

    def clean_conflicts(self, package):
        
        packageName=package[0]
        conflicts=package[1]

        if packageName.startswith('.'):
            print(f'Skipping package {packageName}')
            return

        for conflict in conflicts:
            target =  f'{self.liveRoot}/{conflict["path"]}'
            if(os.path.islink(target)):
                os.unlink(target)
                print(f'Fixed {Style.BRIGHT}{target}{Style.RESET_ALL} {Fore.RED}conflicting symlink removed{Fore.RESET}')
            else:
                os.remove(target)
                print(f'Fixed {Style.BRIGHT}{target}{Style.RESET_ALL} {Fore.RED}conflicting file deleted{Fore.RESET}')

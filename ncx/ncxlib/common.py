import sys, re, os
import json
import urllib3
import requests
import subprocess
import configparser
import tempfile
from os import path, makedirs, environ
from shutil import copyfile

username = None
userhome = None
config = None
configpath = None  # TODO: hide this
ncxpath = None  # Path of the ncx application itself
ncxroot = None  # Path of the ncx repo root


def init(fileName):
    global username
    global userhome
    global config
    global configpath
    global ncxpath
    global ncxroot

    ncxpath = fileName
    ncxroot = os.path.dirname(fileName)

    if 'SUDO_USER' in os.environ:
        username = os.environ['SUDO_USER']
    else:
        username = os.environ['USER']

    userhome = os.path.expanduser(f"~{username}")
    configpath = f"{userhome}/.config/.ncx"

    content = read_file(configpath)
    config = dict(row.split('=') for row in list(filter(None, content.split('\n'))))
    if 'complete' not in config and config['complete'] != 1:
        print(f'Invalid config: {config}')
        exit(0)


def elevate():
    if os.geteuid() != 0:
        print("This command must be run as root; calling via sudo...")
        os.execvp("sudo", ["sudo"] + sys.argv)


def run_command(command):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = p.communicate()
    return output.strip()


def get_config_flag(key):
    try:
        return config[f"flag:{key}"]
    except KeyError:
        return False


def set_config(key: str, value: str):
    # TODO: replace if exists, don't just blind append
    with open(configpath, 'a') as f:
        f.write(f"{key}={value}\n")


def set_config_flag(key: str, value: bool):
    # TODO: replace if exists, don't just blind append
    with open(configpath, 'a') as f:
        f.write(f"flag:{key}={value}\n")


def read_file(file_name):
    with open(os.path.expanduser(file_name), 'r') as f:
        return f.read()


def read_url_to_temp_file(url):
    path = os.path.join(tempfile.mkdtemp(), 'tmp')
    r = requests.get(url, allow_redirects=True)
    open(path, 'wb').write(r.content)
    return path


def prompt_yes_no(question, default=None):
    valid = {"yes": True, "y": True, "no": False, "n": False}
    while True:
        sys.stdout.write(question + ' [y/n]: ')
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Invalid response")


def die(text):
    print(text)
    exit(1)


def clamp(value, minValue, maxValue):
    return max(int(minValue), min(int(maxValue), int(value)))


def safeOpen(filePath):
    makedirs(path.dirname(filePath), exist_ok=True)
    return open(filePath, "w")


def safeCopyFile(path_from, path_to):
    if path.exists(path_to):
        print(f'{path_to} exist, skipping')
        return

    makedirs(path.dirname(path_to), exist_ok=True)
    copyfile(path_from, path_to)


def popArg(args, flags):
    if (not isinstance(flags, (list,))):
        flags = [flags]

    result = False
    for flag in flags:
        if flag in args:
            args.remove(flag)
            result = True
    return result

from ncxlib.common import *
import subprocess

class SwayOsd:

    def runcmd(self,args):
        cmd = f"swayosd {args}"
        result = subprocess.run(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        return {
            "code": result.returncode,
            "stdout": result.stdout.decode().strip(),
            "stderr": result.stderr.rstrip(b'\n').decode().strip()
            }

    def show_backlight(self):
        self.runcmd("--brightness=+0")

    def show_volume(self):
        self.runcmd("--output-volume=+0")

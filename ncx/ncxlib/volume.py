from ncxlib.common import *
from ncxlib.swayosd import SwayOsd
import subprocess


class Volume:
    use_swayosd = get_config_flag('use_swayosd')
    use_swayosd = True # TODO:come back to this maybe

    def __init__(self):
        self.__muted = None
        self.__volume = None
        self.__maxValue = None
        self.__currentValue = None

    def show_current(self):
        print(f"->  {self.current_percentage()}% ({self.__current_value()}/{self.__max_value()})")

    def current_percentage(self):
        return self.__get_volume()

    def current_value(self):
        return self.__current_value()

    def set_absolute(self, value):
        value = self.__clamp(value, 0, 65536) # Maximum value for pactl
        self.__set_volume(value)
        self.__notify()

    def set_percentage(self, value):
        value = self.__clamp(value, 0, 100)
        self.__set_volume(f'{value}%')
        self.__notify()

    def toggle_mute(self):
        self.__toggle_muted()
        self.__notify()

    def __notify(self):
        if self.__use_swayosd():
            SwayOsd().show_volume()

    def __use_swayosd(self):
        # Implement logic here to determine whether to use swayosd or not
        return True

    def __get_volume(self):
        command = "pactl list sinks | grep -m 1 'Volume:' | awk -F'/' '{print $2}' | tr -d ' %'"
        result = subprocess.run(command, shell=True, capture_output=True, text=True)
        output = result.stdout.strip()
        volume_percentage = int(output)
        return volume_percentage

    def __set_volume(self, value):
        if isinstance(value, str) and value.endswith('%'):
            value = value[:-1]  # Remove the % symbol
        subprocess.run(["pactl", "set-sink-volume", "@DEFAULT_SINK@", f"{value}%"])

    def __toggle_muted(self):
        subprocess.run(["pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle"])

    def __min_value(self):
        return 0

    def __max_value(self):
        if self.__maxValue == None:
            self.__maxValue = 65536  # PipeWire uses 0-65536 range
        return self.__maxValue

    def __current_value(self):
        if self.__currentValue == None:
            command = "pactl list sinks | grep -m 1 'Volume:' | awk '{print $3}'"
            result = subprocess.run(command, shell=True, capture_output=True, text=True)
            self.__currentValue = int(result.stdout.strip())
        return self.__currentValue


    @staticmethod
    def __clamp(value, minimum, maximum):
        return max(minimum, min(value, maximum))

import requests, json

class Gitlab:

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def get_auth_token(self):
        authdata= { \
            'grant_type': 'password', \
            'username': self.username, \
            'password': self.password }

        response = requests.post('https://gitlab.com/oauth/token', json=authdata)

        if(not response.status_code == 200):
            raise Exception(f'GitLab auth failed: {response.text}')

        return json.loads(response.text)['access_token']

    def add_ssh_key(self, key_name, public_key):
        header = {'Authorization': f'Bearer {self.get_auth_token()}' }
        data = {'title': key_name, 'key': public_key }
        response = requests.post('https://gitlab.com/api/v4/user/keys', json=data, headers=header)

        if(response.status_code == 201):
            return print(f"Registered SSH public key on GitLab as '{key_name}'")

        result = json.loads(response.text)
        errorMessage=list(result['message'].keys())[0]
        errorDetail = result['message'][errorMessage]
        print(f'Error adding key to GitLab; {errorMessage} {errorDetail}')
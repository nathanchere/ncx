import requests, json

class Github:

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def add_ssh_key(self, key_name, public_key):
        session = requests.session()
        session.auth = (self.username, self.password)
        data = {'title':key_name, 'key':public_key}
        response = session.post('https://api.github.com/user/keys', json=data)

        if(response.status_code == 201):
            print(f"Registered SSH public key on GitHub as '{key_name}'")
            return True

        result = json.loads(response.text)
        errorMessage=result['message']
        print(f'Error adding key to GitHub; {errorMessage}')
        return False

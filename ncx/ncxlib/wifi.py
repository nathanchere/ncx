import NetworkManager #, NMClient
import subprocess

from ncxlib.common import *

# TODO: take this from config
# TODO: set this up on install

class WiFi:

    wifi_device = config['wifi_controller']

    def device(self):
        return wifi_device

    def list_devices(self):
        command = "echo /sys/class/net/*/wireless | awk -F'/' '{ print $5 }'"
        devices = run_command(command)
        return devices.split()

    def list_available_aps(self):
        return print("todo")

    def list_registered_aps(self):
        return print("todo")

    def connect_to_ap(self):
        return print("todo")

    def disconnect(self):
        return subprocess.call(f'nmcli device disconnect iface {wifi_device}', shell=True)

    def reset_interface(self):
        # # sudo lshw -C network 2>&1 | grep wireless | grep driver
        # sudo modprobe -r brcmfmac && sudo modprobe brcmfmac
        print("TODO: work out which way is best")

    def spoof_mac_address(self):
        elevate()
        #TODO: if wifi not disabled, disable it
        subprocess.call(f'macchanger -e {self.wifi_device}', shell=True)
        #TODO: if wifi was disabled here, re-enable it

    def restore_mac_address(self):
        subprocess.call(f'macchanger -p', shell=True)

    def enable(self):
        subprocess.call(f'nmcli radio wifi on', shell=True)

    def disable(self):
        subprocess.call(f'nmcli radio wifi off', shell=True)

    def status(self):
        return print("todo")

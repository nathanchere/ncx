from ncxlib.common import *
import subprocess


class Touchpad:
    def set_tap_to_click(self, isEnable):
        # TODO this doesn't work
        print(f"Setting to {isEnable}")
        subprocess.call(
            f"xinput  set-prop 'SynPS/2 Synaptics TouchPad'  'Synaptic Tap Action' 0", shell=True)

    def __init__(self):
        cmd = r"xinput list-props 'SynPS/2 Synaptics TouchPad' | sed -n -e 's/.*Device Enabled ([0-9][0-9]*):\t\(.*\)/\1/p' "
        result = subprocess.run(
            cmd, stdout=subprocess.PIPE, shell=True).stdout.rstrip(b'\n')
        if(not result == "1"):
            raise Exception("No touchpad found")

from ncxlib.common import *
from ncxlib.swayosd import SwayOsd

class Backlight:
    use_swayosd = get_config_flag('use_swayosd')
    use_swayosd = True # TODO:come back to this maybe
    controller = config['backlight_controller']
    path_max_value = f'{controller}/max_brightness'
    path_current_value = f'{controller}/brightness'

    def show_current(self):
        print(
            f"->  {self.__current_percent()}% ({self.__current_value()}/{self.__max_value()})")

    def current_value(self):
        return self.__current_value()

    def current_percent(self):
        return self.__current_percent()

    def set_max(self):
        print(f"Setting to {self.__max_value()}/{self.__max_value()}")
        self.__set_backlight(self.__max_value())
        self.__notify()

    def set_min(self):
        print(f"Setting to {self.__min_value()}/{self.__max_value()}")
        self.__set_backlight(self.__min_value())
        self.__notify()

    def set_absolute(self, value):
        value = clamp(value, self.__min_value(), self.__max_value())
        print(f"Setting to {value}/{self.__max_value()}")
        self.__set_backlight(value)
        self.__notify()

    def set_percentage(self, value):
        value = clamp(value, 0, 100)
        target = clamp(int(value / 100 * self.__max_value()),
                       self.__min_value(), self.__max_value())
        print(f"Setting to {value}% ({target}/{self.__max_value()})")
        self.__set_backlight(target)
        self.__notify()
        
    def __init__(self):
        self.__maxValue = None
        self.__currentValue = None
        self.__minValue = 2  # TODO: make configurable

    def __notify(self):
        if(self.use_swayosd):
            # pass
            SwayOsd().show_backlight()

    def __set_backlight(self, value):
        with open(Backlight.path_current_value, "w") as f:
            f.write(str(value))

    def __current_percent(self):
        result = self.__current_value() / self.__max_value() * 100
        return int(result)

    def __current_value(self):
        if(self.__currentValue == None):
            with open(Backlight.path_current_value, "r") as f:
                self.__currentValue = int(f.read().strip())
        return self.__currentValue

    def __min_value(self):
        return self.__minValue

    def __max_value(self):
        if(self.__maxValue == None):
            with open(Backlight.path_max_value, "r") as f:
                self.__maxValue = int(f.read().strip())
        return self.__maxValue

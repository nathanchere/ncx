# Very WIP and largely untested

from ncxlib.common import *

class KeyboardBacklight:
    controller='dell::kbd_backlight' #TODO: init on setup and take from ncx config
    path_max_value = f'/sys/class/leds/{controller}/max_brightness'
    path_current_value = f'/sys/class/leds/{controller}/brightness'
    path_timeout = f'/sys/class/leds/{controller}/stop_timeout'

    def show_current_brightness(self):
        if(self.__current_value() == 0):
            value="off"
        else:
            value=f"{self.__current_value()} / {self.__max_value()}"
        print(f"  Backlight: {value}")

    def show_current_timeout(self):
        print(f" * Timeout:   {self.current_timeout()}")

    def current_value(self):
        return self.__current_value()

    def current_timeout(self):
        return self.__current_timeout()

    def set_max(self):
        print(f"Setting to {self.__max_value()} / {self.__max_value()}")
        self.__set_backlight(self.__max_value())

    def set_min(self):
        print(f"Setting to {self.__min_value()} / {self.__max_value()}")
        self.__set_backlight(self.__min_value())

    def set_absolute(self, value):
        value = clamp(value, self.__min_value(), self.__max_value())
        print(f"Setting to {value} / {self.__max_value()}")
        self.__set_backlight(value)

    def set_timeout(self, value):
        print(f"Setting keyboard backlight timeout to {value}")
        self.__set_timeout(value)

    def __init__(self):
        self.max_value = None
        self.current_value = None
        self.min_value = 0

    def __set_backlight(self, value):
        with open(KeyboardBacklight.path_current_value, "w") as f:
            f.write(str(value))

    def __set_timeout(self, value):
        with open(KeyboardBacklight.path_timeout, "w") as f:
            f.write(str(value))

    def __current_timeout(self):
        with open(KeyboardBacklight.path_timeout, "r") as f:
            return f.read().strip()

    def __current_value(self):
        if(self.current_value == None):
            with open(KeyboardBacklight.path_current_value, "r") as f:
                self.current_value = int(f.read().strip())
        return self.current_value

    def __min_value(self):
        return self.min_value

    def __max_value(self):
        if(self.max_value == None):
            with open(KeyboardBacklight.path_max_value, "r") as f:
                self.max_value = int(f.read().strip())
        return self.max_value

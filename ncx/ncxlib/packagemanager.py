import subprocess

class PackageManager:

    def __paru(flags, pkgs=[], eflgs=[]):
        cmd = ["paru", "--noconfirm", flags]

        if pkgs:
            if type(pkgs) != list:
                pkgs = [pkgs]
            cmd += pkgs

        if eflgs and any(eflgs):
            eflgs = [x for x in eflgs if x]
            cmd += eflgs

        cmd=' '.join(cmd)

        # print(f'DEBUG cmd: {cmd}')

        result = subprocess.run(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        return {
            "code": result.returncode,
            "stdout": result.stdout.decode().strip(),
            "stderr": result.stderr.rstrip(b'\n').decode().strip()
            }

    def add_key(self, id, path, sign):
        flag=f"gpg:{id}"
        if(common.get_config_flag(flag)):
            print(f"Already added GPG key '{id}'; skipping")
            return

        print(f"Adding GPG key '{id}'...")
        subprocess.call(f'sudo pacman-key --add {path}', shell=True)
        subprocess.call(f'sudo pacman-key --lsign-key {sign}', shell=True)

        common.set_config_flag(flag)


    def install(self, packages, needed=True):
        print(f"Installing: {packages}")
        s = PackageManager.__paru("-S", packages, ["--needed" if needed else None])
        if s["code"] != 0:
            print("INSTALL FAILED")
            print(f"** STDOUT **:\n{s['stdout']}")
            raise Exception(f"{s['stderr']}")
        result=s['stdout']
        # TODO: Process result
        # if("there is nothing to do" in result): # already installed?


    def refresh(self):
        print("Refreshing info from packages repositories...")
        # Refresh the local package information database
        s = PackageManager.__paru("-Sy")
        if s["code"] != 0:
            raise Exception(f"Failed to refresh database: {s['stderr']}")


    def upgrade(self, packages=[]):
        # Upgrade packages; if unspecified upgrade all packages
        if packages:
            install(packages)
        else:
            s = PackageManager.__paru("-Su")
        if s["code"] != 0:
            raise Exception(f"Failed to upgrade packages: {s['stderr']}")


    def remove(self, packages, purge=False):
        # Remove package(s), purge its files if requested
        s = PackageManager.__paru("-Rc{0}".format("n" if purge else ""), packages)
        if s["code"] != 0:
            raise Exception("Failed to remove: {0}".format(s["stderr"]))


    def get_all(self):
        # List all packages, installed and not installed
        interim, results = {}, []
        s = PackageManager.__paru("-Q")
        if s["code"] != 0:
            raise Exception(
                "Failed to get installed list: {0}".format(s["stderr"])
            )
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            x = x.split(' ')
            interim[x[0]] = {
                "id": x[0], "version": x[1], "upgradable": False,
                "installed": True
            }
        s = PackageManager.__paru("-Sl")
        if s["code"] != 0:
            raise Exception(
                "Failed to get available list: {0}".format(s["stderr"])
            )
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            x = x.split(' ')
            if x[1] in interim:
                interim[x[1]]["repo"] = x[0]
                if interim[x[1]]["version"] != x[2]:
                    interim[x[1]]["upgradable"] = x[2]
            else:
                results.append({
                    "id": x[1], "repo": x[0], "version": x[2], "upgradable": False,
                    "installed": False
                })
        for x, value in interim.items():
            results.append(value)
        return results


    def get_installed():
        # List all installed packages
        interim = {}
        s = PackageManager.__paru("-Q")
        if s["code"] != 0:
            raise Exception(
                "Failed to get installed list: {0}".format(s["stderr"])
            )
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            x = x.split(' ')
            interim[x[0]] = {
                "id": x[0], "version": x[1], "upgradable": False,
                "installed": True
            }
        s = PackageManager.__paru("-Qu")
        if s["code"] != 0 and s["stderr"]:
            raise Exception(
                "Failed to get upgradable list: {0}".format(s["stderr"])
            )
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            x = x.split(' -> ')
            name = x[0].split(' ')[0]
            if name in interim:
                r = interim[name]
                r["upgradable"] = x[1]
                interim[name] = r
        return [value for x, value in interim.items()]


    def get_available(self):
        # List all available packages
        results = []
        s = PackageManager.__paru("-Sl")
        if s["code"] != 0:
            raise Exception(
                "Failed to get available list: {0}".format(s["stderr"])
            )
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            x = x.split(' ')
            results.append({"id": x[1], "repo": x[0], "version": x[2]})
        return results


    def get_info(self, package):
        # Get package information from database
        interim = []
        s = PackageManager.__paru("-Qi" if is_installed(package) else "-Si", package)
        if s["code"] != 0:
            raise Exception("Failed to get info: {0}".format(s["stderr"]))
        for x in s["stdout"].split('\n'):
            if not x.split():
                continue
            if ':' in x:
                x = x.split(':', 1)
                interim.append((x[0].strip(), x[1].strip()))
            else:
                data = interim[-1]
                data = (data[0], data[1] + "  " + x.strip())
                interim[-1] = data
        return {x[0]: x[1] for x in interim}


    def needs_for(self, packages):
        # Get list of not-yet-installed dependencies of these packages
        s = PackageManager.__paru("-Sp", packages, ["--print-format", "%n"])
        if s["code"] != 0:
            raise Exception("Failed to get requirements: {0}".format(s["stderr"]))
        return [x for x in s["stdout"].split('\n') if x]


    def depends_for(self, packages):
        # Get list of installed packages that depend on these
        s = PackageManager.__paru("-Rpc", packages, ["--print-format", "%n"])
        if s["code"] != 0:
            raise Exception("Failed to get depends: {0}".format(s["stderr"]))
        return [x for x in s["stdout"].split('\n') if x]


    def is_installed(self, package):
        # Return True if the specified package is installed
        return PackageManager.__paru("-Q", package)["code"] == 0

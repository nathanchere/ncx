if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="Shows high-level summary of available ncx modules"
help_usage="""
ncx help [module]
  Displays usage information for a specific module
"""

from ncxlib.common import *
import os
from colorama import Fore,Style

help_short="you're reading it now"
#help_usage=""" """

def help(args):
    if (len(args)==0):
        return helpList()

    if(not len(args)==1):
        print("Invalid usage; use: ncx help [module]")
        print("Example:    ncx help backlight")
        print("            ncx help wifi")
        return

    name=args.pop(0)
    usageForModule(name)

def helpForModule(name):
    name_formatted=name.ljust(12,' ')
    try:
        exec(f'import ncx_{name}')
        summary = eval(f'ncx_{name}.help_short')
    except ModuleNotFoundError:
        summary=f'{Fore.RED}*** ERROR ***{Fore.RESET}{Style.DIM} no such module "{name}"{Style.RESET_ALL}'
    except AttributeError:
        summary=f'{Fore.RED}*** ERROR ***{Fore.RESET}{Style.DIM} module not documented{Style.RESET_ALL}'
    print(f'  ncx {name_formatted} {summary}')

def usageForModule(name):
    try:
        exec(f'import ncx_{name}')
        summary = eval(f'ncx_{name}.help_usage')
    except ModuleNotFoundError:
        summary(f' *** ERROR *** no such module "{name}"')
    except AttributeError:
        summary=f'*** ERROR *** module usage not documented'
    print(summary)

def helpList():
    print("Available modules:")

    ncxmodules = [f for f in os.scandir(ncxpath) if not f.is_dir() and f.name[0:4]=='ncx_' and f.name[-3:]=='.py']
    for module in sorted(ncxmodules, key=lambda x:x.name):
        helpForModule(module.name[4:-3])



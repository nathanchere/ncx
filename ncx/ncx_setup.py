if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short = "Configure things like git"
help_usage = """
Usage: ncx setup [package|env]
Set up the specified package. Covers both installation and configuration.

When called with no [package], displays a list of available packages.
When called with [env], exports ncx configuration key/values to $env
"""
import datetime
import socket
import os
import subprocess
import getpass
from ncxlib.common import *
from ncxlib.packagemanager import PackageManager

def setup(args):
    if len(args) == 0:
        return showAvailablePackages()

    arg = args.pop()

    if arg == 'essentials':
        return installEssentials(args)
    if arg == 'xsessions':
        return setupXsessions(args)
    if arg == 'xmonad':
        return installXmonad(args)
    if arg == 'hyprland':
        return installHyprland(args)
    if arg == 'dev':
        return installDevelopment(args)
    if arg == 'git':
        return setupGit(args)
    if arg == 'fonts':
        return setupFonts(args)
    if arg == 'env':
        return exportConfigToEnv(args)
    else:
        print(args)


def exportConfigToEnv(args):
    elevate()
    with open('/etc/profile.d/ncx.env.sh', 'w') as f:
        f.write('#!/usr/bin/env bash\n')
        for key in config.keys():
            envKey = f"NCX_{key}".upper()
            value = config[key]
            print(f"Setting {envKey} to {value}...")
            f.write(f"export {envKey}={value}\n")
    print("...Done")


def showAvailablePackages():
    print("""
Available packages:
* dev
* essentials
  - 
* fonts
  - 
* git
* xmonad
* xsessions
""")


def setupFonts(args):
    # TODO replace specific fonts with AUR
    font_path = os.path.expanduser("~/.local/share/fonts")
    font_source_path = f"{ncxroot}/resources/fonts"
    subprocess.call(
        f'rsync -avm "{font_source_path}" "{font_path}"', shell=True)
    print("Updating font cache...")
    subprocess.call('fc-cache -fv > /dev/null', shell=True)
    # install ttf-mscore-fonts

# Give a list of packages to install, returns the list minus
# ones already installed
def __filter_install_list(packages):
    pm = PackageManager()
    result = []
    for item in packages:
        if(pm.is_installed(item)):
            print(f'Package "{item}" already installed; skipping...')
        else:
            result.append(item)
    return result
    

def installPackages(args, packages, skip_prompt = False):
    to_install = __filter_install_list(packages)
    if(not to_install):
        print("Nothing to install")
        return

    print('This will install:')
    for item in to_install:
        print(f' * {item}')
    if(not skip_prompt and not prompt_yes_no('Continue with installation? (y/n): ')):
        return

    pm = PackageManager()
    pm.refresh()
    for item in to_install:
        pm.install(item)

def installEssentials(args):
    items = ['ruby', 'php', 'perl']
    items += ['paru']
    items += ['downgrade']
    items += ['nomacs', 'vlc', 'acpi']
    items += ['xorg-xinput','wmctrl']
    items += ['xf86-input-synaptics']
    # items += ['polkit-kde-agent']
    items += ['p7zip unrar tar rsync']
    items += ['terminator', 'tmux', 'fish', 'alacritty']
    items += ['xclip']
    items += ['noto-fonts-emoji']
    items += ['diff-so-fancy','lsd','wtfutil']
    items += ['rofi','surfraw']
    items += ['feh', 'dunst', 'compton']
    items += ['nerd-fonts-fira-code', 'nerd-fonts-iosevka','','ttf-nerd-fonts-symbols','ttf-nerd-fonts-symbols-monops -']

    installPackages(args, items)
    print('TODO: after ncx dots stow, configure oh-my-fish with:')
    print('curl -L https://get.oh-my.fish | fish')


def installXmonad(args):
    print('This will install everything for a complete xmonad environment.')
    items = ['xmonad', 'xmonad-utils', 'xmonad-contrib']
    items += ['tint2', 'polybar']
    items += ['picom', 'dunst','blueman','network-manager-applet','xorg-xsetroot','xf86-input-synaptics']
    items += ['xdpyinfo'] # This should be moved as an ncx dependency
    installPackages(args, items)

def installHyprland(args):
    print('This will install everything for a complete hyprland environment.')
    items = ['polkit-kde-agent', 'qt5-wayland', 'qt6-wayland']
    items += ['pipewire', 'wireplumber']
    items += ['hyprpicker', 'hyprland',
        'wl-clipboard','mako',
        'waybar','pyprland']
    items += ['blueman','network-manager-applet']
    installPackages(args, items)


def installDevelopment(args):
    isGitSetup = get_config_flag('git')

    languages = [
        'ruby', 'php', 'perl',
        'elixir', 'erlang-nox',
        'rust',
        'nodejs', 'npm',
        'kotlin',
        'dotnet-runtime', 'dotnet-sdk', 'dotnet-host'
    ]
    editors = ['sublime-text-4', 'vscodium-bin'] #, 'sublime-merge'
    # editors = ['code']
    misc = ['shellcheck']
    # TODO gpg key for sublime needed 8A8F901A

    print('This will install the following languages:')
    for item in languages:
        print(f' * {item}')
    print('and the following editors:')
    for item in editors:
        print(f' * {item}')
    print('and the misc tools:')
    for item in misc:
        print(f' * {item}')
    if(not isGitSetup):
        print('and configure git-related concerns')

    if(not prompt_yes_no('Continue with installation? (y/n): ')):
        exit(0)

    installPackages(args, languages+editors+misc, True)
    

    if(not isGitSetup):
        setupGit(None)

    subprocess.call(
        'sudo ln -sf "/usr/bin/vscodium" "/usr/bin/vscode"', shell=True)

    print('TODO: install sublime merge')
    return


def setupXsessions(args):
    subprocess.call(
        'sudo ln -sf "$HOME/.xmonad/xmonad-start" "/usr/bin/xmonad-start"', shell=True)
    subprocess.call(
        'sudo rm -f /usr/share/xsessions/ncx-*.desktop', shell=True)
    subprocess.call(
        f'sudo rsync -avmi {ncxroot}/system/xsessions/. "/usr/share/xsessions"', shell=True)
    subprocess.call(
        'sudo find /usr/share/xsessions -name "ncx-*.desktop" | sudo xargs chmod 644', shell=True)
    subprocess.call(
        'sudo chown -R root:root "/usr/share/xsessions"', shell=True)


def setupGit(args):
    from ncxlib.gitlab import Gitlab
    from ncxlib.github import Github
    from ncxlib import ssh

    git_name = config['git_name']
    git_email = config['git_email']

    print(f"Setting git user info: '{git_name}' ({git_email})")
    subprocess.call(
        f'git config --global user.email "{git_email}"', shell=True)
    subprocess.call(f'git config --global user.name "{git_name}"', shell=True)
    subprocess.call('git config --global protocol.file.allow always', shell=True)

    host_id = f'{socket.gethostname()}_{datetime.date.today().isoformat()}'

    ssh.generate_keys(git_email)

    githubUser = get_credentials('GitHub')
    if(githubUser):
        github = Github(githubUser[0], githubUser[1])
        github.add_ssh_key(host_id, ssh.get_public_key())

    gitlabUser = get_credentials('GitLab')
    if(gitlabUser):
        gitlab = Gitlab(gitlabUser[0], gitlabUser[1])
        gitlab.add_ssh_key(host_id, ssh.get_public_key())

    set_config_flag("git")


def get_credentials(title):
    print(f"--== {title} SSH Key Registration ==--")
    username = input(f"-> {title} username (leave blank to skip): ")
    if(not username):
        return None
    password = getpass.getpass(f"-> {title} password: ")
    if(not password):
        return None
    return (username, password)

if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short = "local 'pyfiddle' playground for ncx testing only"
# help_usage=""" """

# terminal colour tests

# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL

# from colorama import Fore, Back, Style
# print(Fore.RED + 'some red text')
# print(Back.GREEN + 'and with a green background')
# print(Style.DIM + 'and in dim text')
# print(Style.RESET_ALL)
# print('back to normal now')
# exit(0)

import urwid
from ncxlib.common import *


def show_user():
    # elevate()
    return


def test(args):
    if len(args) == 0:
        return show_user()

    if args[0] == "get":
        return print(get_config_flag(args[1]))

    if args[0] == "set":
        return set_config(args[1], args[2])

    def exit_on_q(key):
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()

    palette = [
        ('banner', '', '', '', '#000', '#BF0'),
        ('streak', '', '', '', 'g50', '#9D0'),
        ('inside', '', '', '', 'g38', '#6A0'),
        ('outside', '', '', '', 'g27', '#380'),
        ('bg', '', '', '', 'g7', '#FFF'), ]

    placeholder = urwid.SolidFill()
    loop = urwid.MainLoop(placeholder, palette, unhandled_input=exit_on_q)
    loop.screen.set_terminal_properties(colors=256)
    loop.widget = urwid.AttrMap(placeholder, 'bg')
    loop.widget.original_widget = urwid.Filler(urwid.Pile([]))

    div = urwid.Divider()
    outside = urwid.AttrMap(div, 'outside')
    inside = urwid.AttrMap(div, 'inside')
    txt = urwid.Text(('banner', u" Hello World "), align='center')
    streak = urwid.AttrMap(txt, 'streak')
    pile = loop.widget.base_widget  # .base_widget skips the decorations
    for item in [outside, inside, streak, inside, outside]:
        pile.contents.append((item, pile.options()))

    loop.run()

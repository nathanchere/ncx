if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="wallpaper helper [INCOMPLETE]"
help_usage="""
Usage: ncx wallpaper [--verbose] <command>

WARNING: not fully implemented yet

Possible commands:
    get (default)     Get a path to a random wallpaper from a pre-defined list

    set               Change the current background wallpaper.
      {filename}      The path to the wallpaper to apply. This can optionally be 
                      left empty, in which case it will choose at random from a
                      pre-defined list of wallpapers.
"""

from ncxlib.common import *
import os,subprocess

def wallpaper(args):
    # from pdb import set_trace
    # set_trace()
    from ncxlib.wallpaper import Wallpaper
    isVerbose = popArg(args, ['--verbose','-v'])

    if(len(args) == 0):
        command = 'get'
    else:
        command = args.pop(0)

    if(command=='get'):            
        return Wallpaper().get()

    if(command=='set'):            
        return Wallpaper().set(args)

    printf("TODO error show help here")
    # TODO: error, show help

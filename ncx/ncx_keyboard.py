if __name__ == "__main__":
    print("Don't try to run this directly")
    exit(1)

help_short="manage keyboard backlight"
#help_usage=""" """

from ncxlib.common import *

def keyboard(args):
    from ncxlib.keyboardbacklight import KeyboardBacklight

    # TODO: make configurable on setup

    backlight = KeyboardBacklight()

    if(len(args) == 0):
        backlight.show_current_brightness()
        backlight.show_current_timeout()
        return

    value = args[0]
    if(value == 'level' or value=='l'):
        if(len(args) == 2):
            return backlight.set_absolute(args[1])
        return backlight.show_current_brightness()

    if(value == 'timeout' or value=='t'):
        if(len(args) == 2):
            return backlight.set_timeout(args[1])
        return backlight.show_current_timeout()


    return showHelp()

def showHelp():
    print("Usage: ncx kb level 5")
    print("       ncx kb timeout 24m")

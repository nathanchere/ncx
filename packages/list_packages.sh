#!/usr/bin/env sh

pacman -eQ | wl-copy # all locall y instaled
pacman -Qqe | grep -v "$(pacman -Qqm)" | wl-copy # only official
pacman -eQm | wl-copy # AUR installed

# install

cat pacman.txt | xargs pacman -S --needed --noconfirm

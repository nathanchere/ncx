#!/usr/bin/env bash

# Stolen from Redhat /etc/profile
# Append to PATH if not already specified
# $1 - string to add to PATH if not already present
# $2 - if 'after', $1 will be added to the end of PATH instead of the start
pathMungeBefore() {
  if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
      PATH=$1:$PATH
  fi
}

pathMungeAfter() {
  if ! echo $PATH | /bin/egrep -q "(^|:)$1($|:)" ; then
      PATH=$PATH:$1
  fi
}

export editor=nano

# don't know why these aren't set sometimes but this works around it
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

pathMungeBefore '/home/norfen/.ncx/system/bin"
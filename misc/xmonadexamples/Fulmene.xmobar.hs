-- https://github.com/Fulmene/dotfiles/tree/master/xmobar/.xmobar

-- datetime

Config {
    -- Appearance
    font = "xft:Noto Mono:size=9:bold:antialias=true" ,
    additionalFonts = [ "xft:FontAwesome:size=10" ] ,
    bgColor = "#262626" ,
    fgColor = "#FFFFDF" ,
    alpha = 204 ,
    position = TopW C 20 ,

    -- Behaviour
    overrideRedirect = False ,
    lowerOnStart = True ,

    template = "}%date%{" ,

    commands = [ Run Date "<fc=#DFAF87><fn=1></fn>%a %b %e</fc> <fn=1></fn>%H:%M" "date" 600 ]
}

-- monitor

Config {
    -- Appearance
    font = "xft:Noto Mono:size=9:bold:antialias=true" ,
    additionalFonts = [ "xft:FontAwesome:size=10" ] ,
    bgColor = "#262626" ,
    fgColor = "#FFFFDF" ,
    alpha = 204 ,
    position = Bottom ,

    -- Behaviour
    overrideRedirect = False ,
    lowerOnStart = True ,

    template = " %uptime% <fc=#87AFAF>%VTBD%</fc>}%cpufreq% %coretemp% %multicpu% %memory% +%swap%{%dynnetwork% %diskio% " ,

    commands =
        [   Run Uptime
            [   "--template", "<fc=#DFAF87><fn=1></fn><days>d</fc> <fc=#FFFFDF><hours>:<minutes></fc>" ,
                "--padchars", "0" ,
                "--width", "2"
            ]
            600 ,

            Run Weather "VTBD"
            [   "--template", "<fn=1></fn><tempC>C <fn=1></fn><skyCondition>"   ]
            9000 ,

            Run MultiCpu
            [   "--template", "<fn=1></fn>[<autototal>]" ,
                "--High", "70" ,
                "--Low", "30" ,
                "--high", "#D75F5F" ,
                "--normal", "#DFAF87" ,
                "--low", "#FFFFDF" ,
                "--suffix", "true" ,
                "--ppad", "3"
            ]
            10 ,

            Run CpuFreq
            [   "--template", "<cpu0>GHz" ,
                "--High", "3" ,
                "--Low", "2" ,
                "--high", "#D75F5F" ,
                "--normal", "#DFAF87" ,
                "--low", "#FFFFDF" ,
                "--ddigits", "2" ,
                "--width", "5"
            ]
            10 ,

            Run CoreTemp
            [   "--template", "<core0>C" ,
                "--High", "84" ,
                "--Low", "50" ,
                "--high", "#D75F5F" ,
                "--normal", "#DFAF87" ,
                "--low", "#FFFFDF"
            ]
            10 ,

            Run Memory
            [   "--template", "<fc=#FFFFDF><fn=1></fn> <used>MB</fc>" ,
                "--High", "4096",
                "--Low", "2048" ,
                "--high", "#D75F5F" ,
                "--normal", "#DFAF87" ,
                "--low", "#FFFFDF" ,
                "--width", "4"
            ]
            10 ,

            Run Swap
            [   "--template", "<used>MB" ,
                "--High", "1024",
                "--Low", "512" ,
                "--high", "#D75F5F" ,
                "--normal", "#DFAF87" ,
                "--low", "#FFFFDF" ,
                "--width", "5"
            ]
            10 ,

            Run DiskIO
            [   ("sda", "<fc=#DFAF87><fn=1></fn> <read>B</fc> <fc=#FFFFDF><fn=1></fn> <write>B</fc>")   ]
            [   "--width", "4"   ]
            10 ,

            Run DynNetwork
            [   "--template", "<fc=#DFAF87><fn=1></fn><tx>KB</fc> <fc=#FFFFDF><fn=1></fn><rx>KB</fc>" ,
                "--width", "4"
            ]
            10

        ]
}

-- status

Config {
    -- Appearance
    font = "xft:Noto Mono:size=9:bold:antialias=true" ,
    additionalFonts = [ "xft:FontAwesome:size=10" ] ,
    bgColor = "#262626" ,
    fgColor = "#FFFFDF" ,
    alpha = 204 ,
    position = TopW R 25 ,

    -- Behaviour
    overrideRedirect = False ,
    lowerOnStart = True ,

    template = "<fc=#87AFAF>%essid%</fc>}{<action=`termite -e neomutt`><fc=#FFFFDF>%mail%</fc></action> <action=`ibus-setup`><fc=#DFAF87><fn=1></fn>%kbd%</fc></action> <action=`xbacklight -inc 5` button=4><action=`xbacklight -dec 5` button=5>%backlight%</action></action> %default:Master% %battery% " ,

    commands =
        [   Run Volume "default" "Master"
            [   "--template", "<action=`amixer -q sset Master toggle` button=1><action=`amixer -q sset Master unmute` button=45><action=`amixer -q sset Master 2dB+` button=4><action=`amixer -q sset Master 2dB-` button=5><status></action></action></action></action>" ,
                "--ppad", "3" ,
                "--" ,
                    "--on", "<fn=1></fn><volume>%" ,
                    "--off", "<fn=1>  </fn>Mute" ,
                    "--onc", "#DFAF87" ,
                    "--offc", "#AF875F"
            ]
            1 ,

            Run Battery
            [   "--template", "<fc=#FFFFDF><fn=1><acstatus></fn><left> (<timeleft>)</fc>" ,
                "--High", "80" ,
                "--Low", "40" ,
                "--high", "#FFFFDF" ,
                "--normal", "#DFAF87" ,
                "--low", "#D75F5F" ,
                "--ppad", "3" ,
                "--suffix", "true" ,
                "--" ,
                    "-O", "" ,
                    "-i", "" ,
                    "-o", "" ,
                    "-f", "AC0/online"
            ]
            50 ,

            Run Kbd
            [   ("us(altgr-intl)", "EN") ,
                ("us", "TH") , -- ibus-libthai uses US layout
                ("th", "TH") ,
                ("jp", "JP") ,
                ("gr", "GR")
            ] ,

            Run Mail
            [   ("<fn=1></fn> ", "~/mail/gmail/INBOX")
            ]
            "mail" ,

            Run Com "getessid" [] "essid" 10 ,

            Run Com "getbacklight" [] "backlight" 1

        ]
}

-- xmonad

Config {
    -- Appearance
    font = "xft:Noto Mono:size=9:bold:antialias=true" ,
    bgColor = "#262626" ,
    fgColor = "#FFFFDF" ,
    alpha = 204 ,
    position = TopW L 40 ,

    -- Behaviour
    overrideRedirect = False ,
    lowerOnStart = True ,

    template = "%UnsafeStdinReader%}{" ,

    commands = [ Run UnsafeStdinReader ]
}
